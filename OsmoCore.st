"
 (C) 2010 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Eval [
    "Handle reloading the OsmoCore.st file for updates"
    Namespace current at: #OSMOCore ifPresent: [ :each |
        OSMOCore logNotice: 'OSMOCore is getting updated.' area: #osmocore.
        OSMOCore stopProcess
    ].
]

Object subclass: OSMOCore [
    <comment: 'I provide lowlevel access libosmocore.so.0'>
    <category: 'OsmoBinding'>

    Process := nil.

    OSMOCore class >> initialize [
        DLD addLibrary: 'libosmocore.so.0'.
        ObjectMemory addDependent: self.
    ]

    OSMOCore class >> poll [
        | delay |
        delay := Delay forMilliseconds: 50.

        [true] whileTrue: [
            self bsc_select_main: 1.
            delay wait.
        ].
    ]

    OSMOCore class >> startProcess [
        "I start a new polling process"

        Process := [
            self poll.
        ] fork.
    ]

    OSMOCore class >> stopProcess [
        "I will terminate the process"
        Process ifNotNil: [
            Process terminate.
            Process := nil.
        ]
    ]

    OSMOCore class >> update: aSymbol [
        "Check if the BSC Symbols are there again?"
    ]

    OSMOCore class >> bsc_select_main: poll [
        <cCall: 'bsc_select_main' returning: #int args: #(#int) >
    ]

    OSMOCore class >> processEvents [
        self bsc_select_main: 1
    ]

    OSMOCore class >> msgb_length: aMsg [
        <cCall: 'msgb_length' returning: #uInt args: #(#cObject) >
    ]

    OSMOCore class >> msgb_data: aMsg [
        <cCall: 'msgb_data' returning: #cObject args: #(#cObject) >
    ]

    OSMOCore class >> msgb_free: aMsg [
        <cCall: 'msgb_free' returning: #void args: #(#cObject) >
    ]

    OSMOCore class >> gsm0480_create_ussd_resp: invokeId trans: transId text: aText [
        <cCall: 'gsm0480_create_ussd_resp' returning: #cObject args: #(#int #int #string) >
    ]

    OSMOCore class >> gsm0808_prepend_dtap_header: msg linkId: aId [
        <cCall: 'gsm0808_prepend_dtap_header' returning: #void args: #(#cObject #uInt) >
    ]
]

Eval [
    OSMOCore initialize.
]
